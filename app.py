from flask import Flask, jsonify, request
import psycopg2
from psycopg2 import sql
from urllib.parse import unquote

app = Flask(__name__)

# Define database connection parameters
dbname = 'gutenberg'
user = 'nishant'
password = 'toomanybooks'
host = 'postgres-server'
# host = "localhost"
port = '5432'


# Define API endpoints
@app.route('/')
def index():
    return 'Welcome to the Project Gutenberg API!'


@app.route('/books', methods=['GET'])
def get_books():
    filters = request.args.to_dict()

    # Set default values for limit and offset parameters
    page = int(filters.pop('page', 1))
    offset = int(page * 25 - 25)

    conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port)
    cur = conn.cursor()

    # Define SQL query
    query = sql.SQL('''
        SELECT  books.gutenberg_id as book_id, 
        lang.code as language, 
        books.title, 
        author.name as author,
        format.mime_type,
        format.url as download_link,
        sub.name as subject,
        bookshelf.name as bookshelf
FROM    books_book books, 
        books_book_languages lang_rel, 
        books_language lang,
        books_author author,
        books_book_authors author_rel,
        books_format format,
        books_book_subjects sub_rel,
        books_subject sub,
        books_book_bookshelves bookshelf_rel,
        books_bookshelf bookshelf
where   books.gutenberg_id = lang_rel.book_id
and     lang_rel.language_id = lang.id
and     books.gutenberg_id = author_rel.book_id
and     author_rel.author_id = author.id
and     books.gutenberg_id = format.book_id
and     books.gutenberg_id = sub_rel.book_id
and     sub_rel.subject_id = sub.id
and     books.gutenberg_id = bookshelf_rel.book_id
and     bookshelf_rel.bookshelf_id = bookshelf.id
    ''')

    filter_values = list()

    # Add filters to the query
    for filter_name, filter_value in filters.items():
        if filter_name == 'id':
            query += sql.SQL(" AND books.gutenberg_id IN ({})").format(
                sql.SQL(',').join(sql.Placeholder() * len(filter_value.split(','))), )
            filter_values = filter_values + filter_value.split(',')
        elif filter_name == 'language':
            query += sql.SQL(" AND lang.code IN ({})").format(
                sql.SQL(',').join(sql.Placeholder() * len(filter_value.split(','))), )
            filter_values = filter_values + filter_value.split(',')
        elif filter_name == 'mime_type':
            query += sql.SQL(" AND format.mime_type IN ({})").format(
                sql.SQL(',').join(sql.Placeholder() * len(filter_value.split(','))), )
            filter_values = filter_values + filter_value.split(',')
        elif filter_name == 'author':
            query += sql.SQL(" AND (")
            filter_value = unquote(filter_value)
            filter_values = filter_values + [f"%{i}%" for i in filter_value.split(',')]
            for i in range(len(filter_value.split(','))):
                if i > 0:
                    query += sql.SQL("OR ")
                query += sql.SQL("author.name ILIKE ({})").format(sql.Placeholder())
            query += sql.SQL(" )")
        elif filter_name == 'title':
            query += sql.SQL(" AND (")
            filter_value = unquote(filter_value)
            filter_values = filter_values + [f"%{i}%" for i in filter_value.split(',')]
            for i in range(len(filter_value.split(','))):
                if i > 0:
                    query += sql.SQL("OR ")
                query += sql.SQL("author.name ILIKE ({})").format(sql.Placeholder())
            query += sql.SQL(" )")
        elif filter_name == 'topic':
            query += sql.SQL(" AND (")
            filter_value = unquote(filter_value)
            topic_filter = [f"%{i}%" for i in filter_value.split(',')]
            filter_values = filter_values + topic_filter
            for i in range(len(filter_value.split(','))):
                if i > 0:
                    query += sql.SQL("OR ")
                query += sql.SQL("sub.name ILIKE ({})").format(sql.Placeholder())
            query += sql.SQL(" )")

            query += sql.SQL(" AND (")
            filter_values = filter_values + topic_filter
            for i in range(len(filter_value.split(','))):
                if i > 0:
                    query += sql.SQL("OR ")
                query += sql.SQL("bookshelf.name ILIKE ({})").format(sql.Placeholder())
            query += sql.SQL(" )")
        else:
            return jsonify({'error': f'Invalid filter parameter: {filter_name}'}), 400


    # Add order by clause
    query += sql.SQL(" ORDER BY books.download_count DESC")

    # Add limit and offset clauses
    query += sql.SQL(" LIMIT {} OFFSET {}").format(sql.Placeholder(), sql.Placeholder())

    filter_values.append(25)
    filter_values.append(offset)
    cur.execute(query, filter_values)

    # Fetch the results
    rows = cur.fetchall()

    # Convert results to list of dictionaries
    books = []
    for row in rows:
        book = {
            'title': row[2],
            'author': row[3],
            'genre': row[6],
            'language': row[1],
            'subject': row[6],
            'bookshelf': row[7],
            'download_links': row[5]
        }
        books.append(book)

    # Count number of books
    count = len(books)

    # Close database connection
    cur.close()
    conn.close()

    # Return results as JSON
    return jsonify({
        'count': count,
        'books': books
    })


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
